import React from 'react';
// import NewResume from './NewResume.pdf'
// import {Document} from 'react-pdf/dist/esm/entry.webpack';
import MyResume from './MyResume.jsx'
import {PDFViewer} from '@react-pdf/renderer';

// const ResumeStyle = {
//   "padding-top": "75px"
// }

function Resume() {
  return (
    <PDFViewer>
      <MyResume />
    </PDFViewer>
  )
}

export default Resume;