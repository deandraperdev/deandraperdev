import React from 'react';
import {Route, Switch, Link} from 'react-router-dom';
import Home from '../Home/Home.jsx';
import Blog from '../Blog/Blog.jsx';
import Resume from '../Resume/Resume.jsx';


import './App.css';

function App() {

  return (
    <>
      <nav className="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
          <div className="container"><Link to="/" className="navbar-brand logo">&lt;deandraper.dev&gt;</Link><button data-bs-toggle="collapse" className="navbar-toggler" data-bs-target="#navcol-1"><span className="visually-hidden">Toggle navigation</span><span className="navbar-toggler-icon"></span></button>
              <div className="collapse navbar-collapse" id="navcol-1">
                  <ul className="navbar-nav ms-auto">
                      <li className="nav-item"><Link to="/home" className="nav-link active">HOME</Link></li>
                      <li className="nav-item"><Link to="/resume" className="nav-link active">RESUME</Link></li>
                      <li className="nav-item"><Link to="/blog" className="nav-link active">BLOG</Link></li>
                  </ul>
              </div>
          </div>
      </nav>
      <div className="push"></div>
      <div className="wrapper">
          <Switch>
            <Route path="/home">
              <Home />
            </Route>
            <Route path="/Resume">
              <Resume />
            </Route>
            <Route path="/blog">
              <Blog />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
      </div>
      <footer className="page-footer dark">
        <div className="footer-copyright">
            <p>© 2021 - Dean Draper</p>
        </div>
    </footer>
    </>
  );
}

export default App;
