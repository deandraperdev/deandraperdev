import { render, screen } from '@testing-library/react';
import App from './App.jsx';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  expect(getByText('Application')).toBeInTheDocument();
  // const linkElement = screen.getByText('Application');
  // expect(linkElement).toBeInTheDocument();
});
